'use strict';

var gulp        = require('gulp');
var sass        = require('gulp-sass');
var criticalCss = require('gulp-penthouse');
var concat      = require('gulp-concat');
var csso        = require('gulp-csso');


var srcPath        = './sass/**/*.scss';
var destPath       = './css';

//let cssNano     = require('gulp-cssnano');

gulp.task('sass', function () {
    return gulp.src(srcPath)
        .pipe(sass().on('error', sass.logError))
        .pipe(csso())
        .pipe(gulp.dest(destPath));
});

gulp.task('sass:watch', function () {
    gulp.watch(srcPath,  gulp.parallel('sass'));
});

gulp.task('css:critical', function () {
    return gulp.src('./sass/master.scss')
        .pipe(criticalCss())
        .pipe(gulp.dest(destPath)); // destination folder for the output file
    return gulp.src('./css/master.css');
});

gulp.task('default', gulp.series('sass', "css:critical")); //'css:critical',

gulp.task('watch', gulp.series('sass', 'sass:watch')); // 'css:critical',
